Feature: Send a message before reject the request → user didnt received a message

    Scenario: log in as user 2
      Given sign in opened page
      When user entered his username "group1" and password "qwe123123"
      And user click on Log In button
      Then user should be routed to home page


    Scenario: user create new group allow subscription
      When user click to the group icon
      And user click to create a new group icon
      And user enter the group name "Auto test group" and the group description "auto test group "
      And user click to the allow subscription checkbox
      And verify the checkbox status for allow subscription
      And user click to the create button
      Then new group should be displayed on the group list

    Scenario: log in as user 1
      Given sign in opened page
      When user entered his username "group" and password "qwe123123"
      When user click on Log In button
      Then user should be routed to home page


   Scenario: join to group with allow subscription permission
      When user click to the group icon
      And user click to join icon
      And user enter group "Auto test group" name on the join group page
      And user click to search icon on the join group page
      And user click to group with allow subscription permission on the join group page
      And user click to join button
      Then verify displayed pop up about request to join to group

   Scenario: log in as user 2
      Given sign in opened page
      When user entered his username "group1" and password "qwe123123"
      And user click on Log In button
      Then user should be routed to home page

   Scenario: send message to group
      When user click to the group icon
      And user click to group
      When user click to the Send button
      And user entered Subject "Test Message Subject" and message "Test Message"
      And user click to the Send icon
      Then user come to feed page
      And new message is displayed

   Scenario: user reject request to join the group
      When user click to feed page
      And user select request to join to the group
      And user click to Reject button
      Then verify displayed the Rejected pop up

   Scenario: log in to user 1
      Given sign in opened page
      When user entered his username "group" and password "qwe123123"
      When user click on Log In button
      Then user should be routed to home page

    Scenario: verify rejected user and message is not displayed
       When user click to the group icon
       Then verify group is not displayed

   #Scenario: verify sent message is not displayed
      When user click to feed page
      Then verify message is not displayed