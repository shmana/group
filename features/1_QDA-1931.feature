@QDA-1932
Feature: 422 send a message before accept the request → user didnt received a message

	Background:
		#@PRECOND_QDA-1900
		Given sign in opened page
		      When user entered his username "group1" and password "qwe123123"
		      And user click on Log In button
		      Then user should be routed to home page
		#@PRECOND_QDA-1901
		When user click to the group icon
		      And user click to create a new group icon
		      And user enter the group name "Auto test group" and the group description "auto test group "
		      And user click to the allow subscription checkbox
		      And verify the checkbox status for allow subscription
		      And user click to the create button
		      Then new group should be displayed on the group list
		#@PRECOND_QDA-1870
		Given sign in opened page
		When user entered his username "group" and password "qwe123123"
		And user click on Log In button
		Then user should be routed to home page
		#@PRECOND_QDA-1902
		When user click to the group icon
		     And user click to join icon
		     And user enter group "Auto test group" name on the join group page
		     And user click to search icon on the join group page
		     And user click to group with allow subscription permission on the join group page
		     And user click to join button
		     Then verify displayed pop up about request to join to group
		#@PRECOND_QDA-1904
		Given sign in opened page
		      When user entered his username "group1" and password "qwe123123"
		      And user click on Log In button
		      Then user should be routed to home page
		#@PRECOND_QDA-1914
		When user click to the group icon
		      And user click to group
		      When user click to the Send button
		      And user entered Subject "Test Message Subject" and message "Test Message"
		      And user click to the Send icon
		      Then user come to feed page
		      And new message is displayed
		#@PRECOND_QDA-1907
		When user click to feed page
		      And user select request to join to the group
		      And user click to Accept button
		      Then verify displayed the Accepted pop up
		      When user click to close button on the pop up
		#@PRECOND_QDA-1908
		Given sign in opened page
		      When user entered his username "group" and password "qwe123123"
		      When user click on Log In button
		      Then user should be routed to home page
		#@PRECOND_QDA-1961
		When user click to the group icon
		       And user click to search icon
		       And user enter group name to the search field
		       Then verify displaying group

	@TEST_QDA-1931
	Scenario: 422 send a message before accept the request → user didnt received a message
		When user click to feed page
		Then verify message is not displayed
