Feature: Sign In
  User should be able to sign in to the system
  Being sign in user is able to use at least default
  set of features of QDA

  Scenario: User typed in correct credentials
    Given sign in opened page
    When user entered his username "group" and password "qwe123123"
    When user click on Log In button
    Then user should be routed to home page