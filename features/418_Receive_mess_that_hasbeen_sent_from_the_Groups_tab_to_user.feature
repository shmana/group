Feature: Send a message to group (with subject and text)
  User should be able to create group allow subscription

  Scenario: Log in as user 1
    Given sign in opened page
    When user entered his username "group" and password "qwe123123"
    When user click on Log In button
    Then user should be routed to home page

    Scenario: User create new group
    When user click to the group icon
    And user click to create a new group icon
    And user enter the group name "Auto test group" and the group description "auto test group "
    And user click to the create button
    Then new group should be displayed on the group list

      Scenario: User invite to group existing user
    When user click to group
    And  user click to the invite members button
    And user click to the Existing User button
    And user search existing user
    And user click to search button
    And user select user 1
    And user click to Add button
    Then Verify displayed pop up about a successful added user
    When user click to close button on the pop up
    Then Verify displayed user on the list

    Scenario: send message to group
      When user click to the group icon
      And user click to group
      When user click to the Send button
      And user entered Subject "Test Message Subject" and message "Test Message"
      And user click to the Send icon
      Then user come to feed page
      And new message is displayed

    Scenario:  Log in to user 2
       Given sign in opened page
       When user entered his username "group1" and password "qwe123123"
       And user click on Log In button
       Then user should be routed to home page

    Scenario: user verify received message
      When user click to feed page
      #When user click to accept message
      When user click to message
      Then new message is displayed
