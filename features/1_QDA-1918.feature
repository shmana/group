@QDA-1921
Feature: 401 Autoapprove: select and join the group from search results -> check if he cannot send a message -> leave group

	Background:
		#@PRECOND_QDA-1900
		Given sign in opened page
		      When user entered his username "group1" and password "qwe123123"
		      And user click on Log In button
		      Then user should be routed to home page
		#@PRECOND_QDA-1919
		When user click to the group icon
		      And user click to create a new group icon
		      And user enter the group name "Auto test group" and the group description "auto test group "
		      And user click to the auto approve checkbox
		      And verify the auto approve checkbox status
		      And user click to the create button
		      Then new group should be displayed on the group list
		#@PRECOND_QDA-1870
		Given sign in opened page
		When user entered his username "group" and password "qwe123123"
		And user click on Log In button
		Then user should be routed to home page
		#@PRECOND_QDA-1920
		When user click to the group icon
		     And user click to join icon
		     And user enter group "Auto test group" name on the join group page
		     And user click to search icon on the join group page
		     And user click to group with auto approve permissions on the join group page
		     And user click to join button
		     Then verify displayed pop up about success join to group
		     When user click to close button on the pop up
		     Then verify displaying joined group with auto approve permissions
		     When user click to group
		     Then verify displayed the Message link
		     And verify if Send link is not displayed

	@TEST_QDA-1918
	Scenario: 401 Autoapprove: select and join the group from search results -> check if he cannot send a message -> leave group
		When user click to group
		     And user click to leave icon on the group page
		     And user click to leave button on the group page
		     Then verify group with auto approve permissions is not displayed
