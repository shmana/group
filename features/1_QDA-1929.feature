@QDA-1930
Feature: 418 Receive mess that has been sent from the Groups tab(existing user)

	Background:
		#@PRECOND_QDA-1870
		Given sign in opened page
		When user entered his username "group" and password "qwe123123"
		And user click on Log In button
		Then user should be routed to home page
		#@PRECOND_QDA-1875
		When user click to the group icon
		And user click to create a new group icon
		And user enter the group name "Auto test group" and the group description "auto test group "
		And user click to the create button
		Then new group should be displayed on the group list
		#@PRECOND_QDA-1897
		When user click to group
		    And  user click to the invite members button
		    And user click to the Existing User button
		    And user search existing user
		    And user click to search button
		    And user select user 1
		    And user click to Add button
		    Then Verify displayed pop up about a successful added user
		    When user click to close button on the pop up
		    Then Verify displayed user on the list
		#@PRECOND_QDA-1914
		When user click to the group icon
		      And user click to group
		      When user click to the Send button
		      And user entered Subject "Test Message Subject" and message "Test Message"
		      And user click to the Send icon
		      Then user come to feed page
		      And new message is displayed
		#@PRECOND_QDA-1900
		Given sign in opened page
		      When user entered his username "group1" and password "qwe123123"
		      And user click on Log In button
		      Then user should be routed to home page

	@TEST_QDA-1929
	Scenario: 418 Receive mess that has been sent from the Groups tab(existing user)
		When user click to feed page
		      And user click to message
		      Then new message is displayed
