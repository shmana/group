@QDA-1893
Feature: 416/1 Invite to group by phone

	Background:
		#@PRECOND_QDA-1870
		Given sign in opened page
		When user entered his username "group" and password "qwe123123"
		And user click on Log In button
		Then user should be routed to home page
		#@PRECOND_QDA-1875
		When user click to the group icon
		And user click to create a new group icon
		And user enter the group name "Auto test group" and the group description "auto test group "
		And user click to the create button
		Then new group should be displayed on the group list

	@TEST_QDA-1891
	Scenario: 416/1 Invite to group by phone
		When user click to group
		    And user click to the invite members button
		    And user click to the By Email or Phone button
		    And user entered valid friends name on the Invite Friends menu
		    And user entered valid phone on the Invite Friends menu
		    And user click to invite button on the Invite Friends menu
		    Then Verify displayed user on the list invited
		    And Verify displayed pop up about a successful added user
