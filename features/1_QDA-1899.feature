@QDA-1905
Feature: 402 request to join the group has been sent to group owner

	Background:
		#@PRECOND_QDA-1900
		Given sign in opened page
		      When user entered his username "group1" and password "qwe123123"
		      And user click on Log In button
		      Then user should be routed to home page
		#@PRECOND_QDA-1901
		When user click to the group icon
		      And user click to create a new group icon
		      And user enter the group name "Auto test group" and the group description "auto test group "
		      And user click to the allow subscription checkbox
		      And verify the checkbox status for allow subscription
		      And user click to the create button
		      Then new group should be displayed on the group list
		#@PRECOND_QDA-1870
		Given sign in opened page
		When user entered his username "group" and password "qwe123123"
		And user click on Log In button
		Then user should be routed to home page
		#@PRECOND_QDA-1902
		When user click to the group icon
		     And user click to join icon
		     And user enter group "Auto test group" name on the join group page
		     And user click to search icon on the join group page
		     And user click to group with allow subscription permission on the join group page
		     And user click to join button
		     Then verify displayed pop up about request to join to group
		#@PRECOND_QDA-1904
		Given sign in opened page
		      When user entered his username "group1" and password "qwe123123"
		      And user click on Log In button
		      Then user should be routed to home page

	@TEST_QDA-1899
	Scenario: 402 request to join the group has been sent to group owner
		When user click to feed page
		      And user select request to join to the group
		      Then displaying the Accept button on the request message
		      And displaying the Reject button on the request message
