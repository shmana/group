Feature: create group with auto approve option
  User should be able to create group with auto approve option

    Scenario: log in
    Given sign in opened page
    When user entered his username "group" and password "qwe123123"
    And user click on Log In button
    Then user should be routed to home page

      Scenario: user create new group allow subscription
      When user click to the group icon
      And user click to create a new group icon
      And user enter the group name "Auto test group" and the group description "auto test group"
      And user click to the auto approve checkbox
      And verify the auto approve checkbox status
      And user click to the create button
      Then new group should be displayed on the group list

