@QDA-1898
Feature: 417 Delete member from the group 

	Background:
		#@PRECOND_QDA-1870
		Given sign in opened page
		When user entered his username "group" and password "qwe123123"
		And user click on Log In button
		Then user should be routed to home page
		#@PRECOND_QDA-1875
		When user click to the group icon
		And user click to create a new group icon
		And user enter the group name "Auto test group" and the group description "auto test group "
		And user click to the create button
		Then new group should be displayed on the group list
		#@PRECOND_QDA-1897
		When user click to group
		    And  user click to the invite members button
		    And user click to the Existing User button
		    And user search existing user
		    And user click to search button
		    And user select user 1
		    And user click to Add button
		    Then Verify displayed pop up about a successful added user
		    When user click to close button on the pop up
		    Then Verify displayed user on the list
		#@PRECOND_QDA-1954
		When user click to group
		    And  user click to the invite members button
		    And user click to the Existing User button
		    And user search existing user 2
		    And user click to search button
		    And user select user 2
		    And user click to Add button
		    Then Verify displayed pop up about a successful added user
		    When user click to close button on the pop up
		    Then Verify displayed user 2 on the list

	@TEST_QDA-1896
	Scenario: 417 Delete member from the group 
		When user click to delete icon on the members page
		    And user confirm delete member from a group
		    Then verify displaying the user 2
		    And verify not displaying the user 1
