@QDA-1890
Feature: 416 Invite to group by email

	Background:
		#@PRECOND_QDA-1870
		Given sign in opened page
		When user entered his username "group" and password "qwe123123"
		And user click on Log In button
		Then user should be routed to home page
		#@PRECOND_QDA-1875
		When user click to the group icon
		And user click to create a new group icon
		And user enter the group name "Auto test group" and the group description "auto test group "
		And user click to the create button
		Then new group should be displayed on the group list
		#@PRECOND_QDA-1956
		When user click to group
		    And user click to the invite members button
		    And user click to the By Email or Phone button
		    And user entered valid friends name on the Invite Friends menu
		    And user entered valid email on the Invite Friends menu
		    And user click to invite button on the Invite Friends menu
		    Then Verify displayed user on the list invited
		    And verify displayed the pending text
		    And Verify displayed pop up about a successful added user
		#@PRECOND_QDA-1957
		Given user user open gmail authorization page
		    When user entered the valid mail "Autotests.quicklert@gmail.com" on the gmail authorization page
		    And user click to the next button on the gmail authorization page
		    And user entered the valid pass "Autotests911" on the gmail authorization page
		    And user click to the next button on the gmail authorization page
		#@PRECOND_QDA-1958
		Given user open the gmail mail page
		    When user click to the received message
		    And user click to accept button on the gmail
		    Then verify displaying the group subscribe page
		#@PRECOND_QDA-1959
		When user enter group subscription data
		    Then verify displaying the data on the email input
		    When user click to Subscribe button
		    Then verify displaying successful text
		    And verify displaying the successful pop up
		#@PRECOND_QDA-1908
		Given sign in opened page
		      When user entered his username "group" and password "qwe123123"
		      When user click on Log In button
		      Then user should be routed to home page

	@TEST_QDA-1889
	Scenario: 416 Invite to group by email
		When user click to the group icon
		    And user click to group
		    And user click to the Members tab
		    Then Verify displayed user on the list invited
		    And verify not displayed the pending test
