Feature: 403 edit group (change name, description, picture)


  Scenario: User typed in correct credentials
    Given sign in opened page
    When user entered his username "group" and password "qwe123123"
    When user click on Log In button
    Then user should be routed to home page

  Scenario: User create new group
    When user click to the group icon
    And user click to create a new group icon
    And user enter the group name "Auto test group" and the group description "auto test group "
    And user click to the create button
    Then new group should be displayed on the group list

  Scenario: User edit group
    When user click to the group icon
    And user click to group
    And user click to Edit icon
    And user enter new group name on the edit window
    And user enter new group description on the edit window
    And user upload picture on the edit window
    And user save image for avatar
    And user save edited  group
    Then verify displaying new group name
    And verify displaying new Group description
    And verify displaying new avatar
