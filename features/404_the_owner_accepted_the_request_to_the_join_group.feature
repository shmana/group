Feature: Request to join the group has been sent to group owner

   Scenario:  Log in to user 2
      Given sign in opened page
      When user entered his username "group1" and password "qwe123123"
      And user click on Log In button
      Then user should be routed to home page

  Scenario: user create new group allow subscription
      When user click to the group icon
      And user click to create a new group icon
      And user enter the group name "Auto test group" and the group description "auto test group "
      And user click to the allow subscription checkbox
      And verify the checkbox status for allow subscription
      And user click to the create button
      Then new group should be displayed on the group list

  Scenario: log in to user 1
    Given sign in opened page
    When user entered his username "group" and password "qwe123123"
    When user click on Log In button
    Then user should be routed to home page


   Scenario: join to group with allow subscription permission
     When user click to the group icon
     And user click to join icon
     And user enter group "Auto test group" name on the join group page
     And user click to search icon on the join group page
     And user click to group with allow subscription permission on the join group page
     And user click to join button
     Then verify displayed pop up about request to join to group

    Scenario:  Log in to user 2
      Given sign in opened page
      When user entered his username "group1" and password "qwe123123"
      And user click on Log In button
      Then user should be routed to home page

    Scenario: user accept request to join the group
      When user click to feed page
      And user select request to join to the group
      And user click to Accept button
      Then verify displayed the Accepted pop up
      When user click to close button on the pop up
    Scenario: log in to user 1
      Given sign in opened page
      When user entered his username "group" and password "qwe123123"
      When user click on Log In button
      Then user should be routed to home page

    Scenario: Search a shared group
       When user click to the group icon
       And user click to search icon
       And user enter group name to the search field
       Then verify displaying group


