Feature: Share group with execute permission -> accept -> check for group


    Scenario: Log in to user 1
   Given sign in opened page
    When user entered his username "group" and password "qwe123123"
    When user click on Log In button
    Then user should be routed to home page

    Scenario: User create new group
    When user click to the group icon
    And user click to create a new group icon
    And user enter the group name "Auto test group" and the group description "auto test group "
    And user click to the create button
    Then new group should be displayed on the group list

    Scenario: Share group with execute permission
       When user click to group
       And user click to share many on the group page
       And user click to share icon on the group page
       And user enter the user data "group1 group1"
       And user click to search icon on the share menu
       And user click to execute checkbox on the share menu
       Then verify checkboxes on the share menu with execute permission
       When user click to share button on the share menu
       Then verify displayed info about shared to user2 from user1 side

    Scenario:  Log in to user 2
       Given sign in opened page
       When user entered his username "group1" and password "qwe123123"
       And user click on Log In button
       Then user should be routed to home page

    #Scenario: Accept a shared group
    #   When user click to feed page
    #   And user click to message
    #   And  user click to Accept button
    #   Then verify displayed the Accepted pop up

   Scenario: user accept request to join the group
      When user click to feed page
      And user select request to join to the group
      And user click to Accept button
      Then verify displayed the Accepted pop up
      When user click to close button on the pop up
    Scenario: Search a shared group
       When user click to the group icon
       And user click to search icon
       And user enter group name to the search field
       Then verify displaying group
       And verify displaying the share icon

    Scenario: Search a shared group on my tab
       When user click to the group icon
       And user click to the my tab
       And user click to search icon
       And user enter group name to the search field
       Then verify displaying group
       And verify displaying the share icon

   Scenario: verify execute permission
      When user click to group
      Then verify displayed the Send link
      And verify displayed the Message link
      And verify displayed the Members link
      And verify displayed the Dynamic Rules link
      And verify displayed the QR icon