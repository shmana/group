@QDA-1966
Feature: 426 Invite to group by phone (Scenario outline)

		Scenario:
  #@PRECOND_QDA-1870
		Given sign in opened page
		When user entered his username "group" and password "qwe123123"
		And user click on Log In button
		Then user should be routed to home page
		#@PRECOND_QDA-1875
		Scenario:
		When user click to the group icon
		And user click to create a new group icon
		And user enter the group name "Auto test group" and the group description "auto test group "
		And user click to the create button
		Then new group should be displayed on the group list

	@TEST_QDA-1965
	Scenario Outline: 426 Invite to group by phone (Scenario outline)
		When user click to group
		    And user click to the invite members button
		    And user click to the By Email or Phone button
		    And user entered valid friends name on the Invite Friends menu
		    And user entered valid phone <phone> on the Invite Friends menu (Outline)
		    Then Verify displayed error text for phone input
		    And Verify disabled the Invite button
		    When user entered valid phone <phone1> on the Invite Friends menu (Outline)
		    Then Verify disabled the Invite button
		    When user entered empty phone data
		    Then Verify disabled the Invite button
		    When user entered long value to the phone input
		    Then verify value on the phone input
		      Examples:
		        | phone    | phone1   |
		        | 1        | abc      |
		        | 11       | QWE      |
		        | 111      | !-@#$%   |
		        | 1111     | ^&*()    |
		        | 11111    | =_+,.    |
		        | 111111   | '"<>:    |
		        | 1111111  | \/;№{}[] |
		        | 11111111 | spaces   |
