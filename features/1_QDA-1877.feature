@QDA-1878
Feature: 421 Delete group 

	Background:
		#@PRECOND_QDA-1870
		Given sign in opened page
		When user entered his username "group" and password "qwe123123"
		And user click on Log In button
		Then user should be routed to home page
		#@PRECOND_QDA-1875
		When user click to the group icon
		And user click to create a new group icon
		And user enter the group name "Auto test group" and the group description "auto test group "
		And user click to the create button
		Then new group should be displayed on the group list

	@TEST_QDA-1877
	Scenario: 421 Delete group 
		When user click to delete icon on the group page
		    And user confirm delete group
		    Then verify group is not displayed
