Feature: new group
  User should be able to sign in to the system
  Being sign in user is able to use at least default
  set of features of QDA

  Scenario: User typed in correct credentials
    Given sign in opened page
    When user entered his username "group" and password "qwe123123"
    When user click on Log In button
    Then user should be routed to home page

    Scenario: User create new group
    When user click to the group icon
    And user click to create a new group icon
    And user enter the group name "Auto test group" and the group description "auto test group "
    And user click to the create button
    Then new group should be displayed on the group list

    Scenario: user verify displayed tabs
     When user click to group
     Then verify displayed the Send link
     And verify displayed the Message link
     And verify displayed the Members link
     And verify displayed the Share With link
     And verify displayed the Dynamic Rules link
     And verify displayed the edit group icon
     And verify displayed the invite friends icon
     And verify displayed the delete group icon
     And verify displayed the QR icon
