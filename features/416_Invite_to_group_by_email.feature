Feature: new group
  User should be able to sign in to the system
  Being sign in user is able to use at least default
  set of features of QDA

  Scenario: User typed in correct credentials
    Given sign in opened page
    When user entered his username "group" and password "qwe123123"
    When user click on Log In button
    Then user should be routed to home page

  Scenario: User create new group
    When user click to the group icon
    And user click to create a new group icon
    And user enter the group name "Auto test group" and the group description "auto test group "
    And user click to the create button
    Then new group should be displayed on the group list

  Scenario: User invite to group existing user by email
    When user click to group
    And user click to the invite members button
    And user click to the By Email or Phone button
    And user entered valid friends name on the Invite Friends menu
    And user entered valid email on the Invite Friends menu
    And user click to invite button on the Invite Friends menu
    Then Verify displayed user on the list invited
    And verify displayed the pending text
    And Verify displayed pop up about a successful added user

  Scenario: authorization Google

    Given user user open gmail authorization page
    When user entered the valid mail "Autotests.quicklert@gmail.com" on the gmail authorization page
    And user click to the next button on the gmail authorization page
    And user entered the valid pass "Autotests911" on the gmail authorization page
    And user click to the next button on the gmail authorization page

  Scenario: user accept on the gmail
    Given user open the gmail mail page
    When user click to the received message
    And user click to accept button on the gmail
    Then verify displaying the group subscribe page

  Scenario: user entered myself data
    When user enter group subscription data
    Then verify displaying the data on the email input
    When user click to Subscribe button
    Then verify displaying successful text
    And verify displaying the successful pop up

  Scenario: User typed in correct credentials
    Given sign in opened page
    When user entered his username "group" and password "qwe123123"
    When user click on Log In button
    Then user should be routed to home page

  Scenario: user verify invited user
    When user click to the group icon
    And user click to group
    And user click to the Members tab
    Then Verify displayed user on the list invited
    And verify not displayed the pending test


