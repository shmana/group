@QDA-1964
Feature: 425 Invite to group by email (Scenario outline)

	Scenario:
		#@PRECOND_QDA-1870
		Given sign in opened page
		When user entered his username "group" and password "qwe123123"
		And user click on Log In button
		Then user should be routed to home page
		#@PRECOND_QDA-1875
	Scenario:
		When user click to the group icon
		And user click to create a new group icon
		And user enter the group name "Auto test group" and the group description "auto test group "
		And user click to the create button
		Then new group should be displayed on the group list

	@TEST_QDA-1963
	Scenario Outline: 425 Invite to group by email (Scenario outline)
		When user click to group
		    And user click to the invite members button
		    And user click to the By Email or Phone button
		    And user entered valid friends name on the Invite Friends menu
		    And user entered invalid email <email> on the Invite Friends menu (Outline)
		    Then Verify displayed error text for email input
		    And Verify disabled the Invite button
		    When user entered invalid email <email1> on the Invite Friends menu (Outline)
		    Then Verify disabled the Invite button
		    And Verify displaying max length
		    When user entered invalid email <email2> on the Invite Friends menu (Outline)
		    Then Verify disabled the Invite button
		    When user entered empty email data
		
		
		
		      Examples:
		        | email        | email1                                                  |
		        | Test         | qazwsxedcrfvtgbyhnujmikolp1234567890-=_+[]{}';":/.,<>?  |
		        | Test@        | 123456789012345678901234567890123456789012345678901     |
		        | Test@gmail   | qazwsxedcrfvtgbyhnujmikolpqazwsxedcrfvtgbyhnujmikolp    |
		        | Test.com     | !@#$%^&*()[]{};':",./<>?№;:?!@#$%^&*()[]{};':",./<>?№;: |
		        | 1234567890   | 1234567890-=_+[]{}';":/.,<>?qazwsxedcrfvtgbyhnujmikolp  |
		        | !@#$%^&*()_+ | -=_+[]{}';":/.,<>?qazwsxedcrfvtgbyhnujmikolp1234567890  |
		        | spaces       | -=_+[]{}';":/.,<>?qazwsxedcrfvtgbyhnujmikolp1234567890  |
