Feature: Send a message to group (with subject and text)
  User should be able to create group allow subscription

  Scenario: User typed in correct credentials
    Given sign in opened page
    When user entered his username "group" and password "qwe123123"
    When user click on Log In button
    Then user should be routed to home page

    Scenario: User create new group
    When user click to the group icon
    And user click to create a new group icon
    And user enter the group name "Auto test group" and the group description "auto test group "
    And user click to the create button
    Then new group should be displayed on the group list

    Scenario: send message to group

      When user click to the group icon
      And user click to group
      When user click to the Send button
      And user entered Subject "Test Message Subject" and message "Test Message"
      And user click to the Send icon
      Then user come to feed page
      And new message is displayed
      When user click to the group icon
      And user click to group
      And user click to the Message tab
      Then new message is displayed for the Message tab