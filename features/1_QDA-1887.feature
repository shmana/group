@QDA-1888
Feature: 415 Invite to group existing group

	Background:
		#@PRECOND_QDA-1870
		Given sign in opened page
		When user entered his username "group" and password "qwe123123"
		And user click on Log In button
		Then user should be routed to home page
		#@PRECOND_QDA-1875
		When user click to the group icon
		And user click to create a new group icon
		And user enter the group name "Auto test group" and the group description "auto test group "
		And user click to the create button
		Then new group should be displayed on the group list

	@TEST_QDA-1887
	Scenario: 415 Invite to group existing group
		When user click to group
		    And user click to the invite members button
		    And user click to the Existing Group button
		    And user search existing group
		    And user click to search button
		    And user select the Chernihiv Office group
		    And user click to Add button
		    Then verify displayed pop up about a successful added group
		    And verify displayed group on the list
