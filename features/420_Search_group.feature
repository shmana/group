#noinspection CucumberPlusUndefinedStep
Feature: Search group
  User should be able to create group allow subscription

  Scenario: User typed in correct credentials
    Given sign in opened page
    When user entered his username "group" and password "qwe123123"
    When user click on Log In button
    Then user should be routed to home page

    Scenario: User create new group
    When user click to the group icon
    And user click to create a new group icon
    And user enter the group name "Auto test group" and the group description "auto test group "
    And user click to the create button
    Then new group should be displayed on the group list

  Scenario: search group
    When user click to the Group icon
    And user click to the all tab
    And user click to search icon
    And user enter group name to the search field
    Then verify displaying group
    When user click to the group icon
    When user click to the my tab
    And user click to search icon
    And user enter group name to the search field
    Then verify displaying group