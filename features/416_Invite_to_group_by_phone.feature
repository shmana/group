Feature: new group
  User should be able to sign in to the system
  Being sign in user is able to use at least default
  set of features of QDA

  Scenario: User typed in correct credentials
    Given sign in opened page
    When user entered his username "group" and password "qwe123123"
    When user click on Log In button
    Then user should be routed to home page

    Scenario: User create new group
    When user click to the group icon
    And user click to create a new group icon
    And user enter the group name "Auto test group" and the group description "auto test group "
    And user click to the create button
    Then new group should be displayed on the group list

    Scenario: User invite to group existing user by phone
    When user click to group
    And user click to the invite members button
    And user click to the By Email or Phone button
    And user entered valid friends name on the Invite Friends menu
    And user entered valid phone on the Invite Friends menu
    And user click to invite button on the Invite Friends menu
    Then Verify displayed user on the list invited
    And Verify displayed pop up about a successful added user