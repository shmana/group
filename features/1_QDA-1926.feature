@QDA-1928
Feature: 410 Share group with edit permission -> accept -> check for group 

	Background:
		#@PRECOND_QDA-1870
		Given sign in opened page
		When user entered his username "group" and password "qwe123123"
		And user click on Log In button
		Then user should be routed to home page
		#@PRECOND_QDA-1875
		When user click to the group icon
		And user click to create a new group icon
		And user enter the group name "Auto test group" and the group description "auto test group "
		And user click to the create button
		Then new group should be displayed on the group list
		#@PRECOND_QDA-1927
		When user click to group
		       And user click to share many on the group page
		       And user click to share icon on the group page
		       And user enter the user data "group1 group1"
		       And user click to search icon on the share menu
		       And user click to edit checkbox on the share menu
		       Then verify checkboxes on the share menu with edit permission
		       When user click to share button on the share menu
		       Then verify displayed info about shared to user2 from user1 side
		#@PRECOND_QDA-1900
		Given sign in opened page
		      When user entered his username "group1" and password "qwe123123"
		      And user click on Log In button
		      Then user should be routed to home page
		#@PRECOND_QDA-1907
		When user click to feed page
		      And user select request to join to the group
		      And user click to Accept button
		      Then verify displayed the Accepted pop up
		      When user click to close button on the pop up
		#@PRECOND_QDA-1924
		When user click to the group icon
		      And user click to search icon
		      And user enter group name to the search field
		      Then verify displaying group
		      And verify displaying the share icon
		#@PRECOND_QDA-1953
		When user click to the group icon
		       And user click to the my tab
		       And user click to search icon
		       And user enter group name to the search field
		       Then verify displaying group
		       And verify displaying the share icon

	@TEST_QDA-1926
	Scenario: 410 Share group with edit permission -> accept -> check for group 
		When user click to group
		       Then verify displayed the Send link
		       And verify displayed the Message link
		       And verify displayed the Members link
		       And verify displayed the Share With link
		       And verify displayed the Dynamic Rules link
		       And verify displayed the edit group icon
		       And verify displayed the invite friends icon
		       And verify displayed the QR icon
		       When user click to Edit icon
		       Then verify displaying the save button
		       And verify displaying the group name input
		       And verify displaying the group description input
		       And verify displaying the change picture permission
		       And verify displaying the Allow Subscription checkbox
		       And verify displaying the Auto Approve checkbox
