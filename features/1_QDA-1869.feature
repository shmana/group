@QDA-1871
Feature: create group with allow subscription option

	Background:
		#@PRECOND_QDA-1870
		Given sign in opened page
		When user entered his username "group" and password "qwe123123"
		And user click on Log In button
		Then user should be routed to home page

	@TEST_QDA-1869
	Scenario: 412 create group with allow subscription option
		When user click to the group icon
		And user click to create a new group icon
		And user enter the group name "Auto test group" and the group description "auto test group "
		And user click to the allow subscription checkbox
		And verify the checkbox status for allow subscription
		And user click to the create button
		Then new group should be displayed on the group list
