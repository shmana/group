import time

from behave import *

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By

@given("sign in opened page")
def load_login_page(context):
    context.browser.get("https://noctdips03.quicklert.com/connect/#/auth/login")
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.ID, 'username')))



@when('user entered his username "{username}" and password "{password}"')
def step_impl(context, username, password):
    time.sleep(1)
    context.browser.find_element_by_id("username").clear()
    assert context.browser.find_element_by_id("username")
    context.browser.find_element_by_id("username").send_keys(username)

    context.browser.find_element_by_id("password").clear()
    assert context.browser.find_element_by_id("password")
    context.browser.find_element_by_id("password").send_keys(password)


@when('user click on Log In button')
def user_click_login_button(context):
    submitButton = context.browser.find_element_by_class_name("q-btn-default")
    assert submitButton
    submitButton.click()

@then("user should be routed to home page")
def check_if_home_page(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.ID, 'messages')))
    assert context.browser.find_elements_by_xpath("//*[contains(text(), 'All Quicklerts')]")