import random
import time
#import pyautogui

from behave import *
from hamcrest import assert_that, equal_to

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By

new_count = random.uniform(0, 5)


#############################################################################################################
##########################################User create new group##############################################
#############################################################################################################

@when(u'user click to the group icon')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.ID, 'groups-link')))
    submitButton = context.browser.find_element_by_id("groups-link")
    assert submitButton
    submitButton.click()
    #time.sleep(5)

@when(u'user click to create a new group icon')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '//*[@id="contacts"]/div/div[1]/div[1]/div/div[1]/div[2]')))
    submitButton = context.browser.find_element_by_xpath ('//*[@id="contacts"]/div/div[1]/div[1]/div/div[1]/div[2]')
    assert submitButton
    submitButton.click()
    #time.sleep(1)

@when(u'user enter the group name "{group_name}" and the group description "{group_desc}"')
def step_impl(context, group_name, group_desc):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.ID, 'group-name')))
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.ID, 'group-desc')))

    context.browser.find_element_by_id("group-name").clear()
    assert context.browser.find_element_by_id("group-name")
    context.browser.find_element_by_id("group-name").send_keys(group_name, " " + str(new_count))

    context.browser.find_element_by_id("group-desc").clear()
    assert context.browser.find_element_by_id("group-desc")
    context.browser.find_element_by_id("group-desc").send_keys(group_desc)

@when(u'user click to the create button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[1]/div/div[2]/div[2]/div/div[2]/form/div[1]/button')))

    submitButton = context.browser.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[2]/div/div[2]/form/div[1]/button')
    assert submitButton
    submitButton.click()
    #time.sleep(2)



@when(u'user click to group')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
    EC.presence_of_element_located((By.XPATH, "//*[text() = 'Auto test group " + str(new_count) + "']")))
    submitButton = context.browser.find_element_by_xpath("//*[text() = 'Auto test group " + str(new_count) + "']")
    assert submitButton
    submitButton.click()
    #time.sleep(5)

@when(u'user click to the invite members button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/div[2]/div[2]")))

    submitButton = context.browser.find_element_by_xpath(
        "/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/div[2]/div[2]")
    assert submitButton
    submitButton.click()

@when(u'user click to the Existing User button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[text() = 'Existing User']")))
    submitButton = context.browser.find_element_by_xpath("//*[text() = 'Existing User']")
    assert submitButton
    submitButton.click()

@when(u'user search existing user')
def step_impl(context):
    context.browser.find_element_by_id("search-user").clear()
    assert context.browser.find_element_by_id("search-user")
    context.browser.find_element_by_id("search-user").send_keys("group1 group1")
@when(u'user search existing user 2')
def step_impl(context):
    context.browser.find_element_by_id("search-user").clear()
    assert context.browser.find_element_by_id("search-user")
    context.browser.find_element_by_id("search-user").send_keys("group2 group2")

@when(u'user click to search button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[2]/div/div[2]/div/div[1]/div/img")))
    submitButton = context.browser.find_element_by_xpath(
        "/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[2]/div/div[2]/div/div[1]/div/img")
    assert submitButton
    submitButton.click()

@when(u'user select user 1')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[text() = 'group1 group1']")))
    submitButton = context.browser.find_element_by_xpath("//*[text() = 'group1 group1']")
    assert submitButton
    submitButton.click()

@when(u'user select user 2')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[text() = 'group2 group2']")))
    submitButton = context.browser.find_element_by_xpath("//*[text() = 'group2 group2']")
    assert submitButton
    submitButton.click()

@when(u'user click to Add button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located(
        (By.XPATH, "/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/button")))
    submitButton = context.browser.find_element_by_xpath(
        "/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/button")
    assert submitButton
    submitButton.click()
    # time.sleep(2)

@then(u'Verify displayed pop up about a successful added user')
def step_impl(context):

        WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'div[class = "b-toast b-toast-append b-toast-success"]')))
        assert context.browser.find_element_by_css_selector('div[class = "b-toast b-toast-append b-toast-success"]')

@then(u'Verify displayed pop up about a successful added group')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[text() = 'Groups added successfuly']")))
    assert context.browser.find_element_by_xpath("//*[text() = 'Groups added successfuly']")

@then(u'Verify displayed user on the list')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, '//*[@id="group-tabs"]//div[text() = "Group1 Group1"]')))
    assert context.browser.find_element_by_xpath('//*[@id="group-tabs"]//div[text() = "Group1 Group1"]')

@then(u'Verify displayed user 2 on the list')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, '//*[@id="group-tabs"]//div[text() = "Group2 Group2"]')))
    assert context.browser.find_element_by_xpath('//*[@id="group-tabs"]//div[text() = "Group2 Group2"]')

@when(u'user search existing group')
def step_impl(context):
        context.browser.find_element_by_id("group-name").clear()
        assert context.browser.find_element_by_id("group-name")
        context.browser.find_element_by_id("group-name").send_keys("Chernihiv office")
@when(u'user click to the Existing Group button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[text() = 'Existing Group']")))
    submitButton = context.browser.find_element_by_xpath("//*[text() = 'Existing Group']")
    assert submitButton
    submitButton.click()

@when(u'user select the Chernihiv Office group')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//span[text() = 'Chernihiv Office']")))
    submitButton = context.browser.find_element_by_xpath("//span[text() = 'Chernihiv Office']")
    assert submitButton
    submitButton.click()



@then(u'verify displayed group on the list')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, '//*[@id="group-tabs"]//div[text() = "Chernihiv Office"]')))
    assert context.browser.find_element_by_xpath('//*[@id="group-tabs"]//div[text() = "Chernihiv Office"]')

@when(u'user click to the allow subscription checkbox')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[text() = 'Allow Subscription']")))
    context.browser.find_element_by_xpath("//*[text() = 'Allow Subscription']").click()

@when(u'verify the checkbox status for allow subscription')
def step_impl(context):
    assert context.browser.find_element_by_id('auto-subscribe-checkbox').is_selected() == True
    assert context.browser.find_element_by_id('auto-approve-checkbox').is_selected() == False

@when(u'user click to the auto approve checkbox')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[text() = 'Auto Approve Subscribers']")))
    context.browser.find_element_by_xpath("//*[text() = 'Auto Approve Subscribers']").click()


@when(u'verify the auto approve checkbox status')
def step_impl(context):
    assert context.browser.find_element_by_id('auto-subscribe-checkbox').is_selected() == True
    assert context.browser.find_element_by_id('auto-approve-checkbox').is_selected() == True

@when(u'User click to QR icon')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH, '/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/div[2]/div[3]/div')))
    submitButton = context.browser.find_element_by_xpath('/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/div[2]/div[3]/div')
    assert submitButton
    submitButton.click()

@then(u'verify trigger QR Code')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.visibility_of_element_located((By.ID, "qrCodeSubscribeGroupModal___BV_modal_body_")))
    assert context.browser.find_element_by_id("qrCodeSubscribeGroupModal___BV_modal_body_")

    WebDriverWait(context.browser, 20).until(
        EC.visibility_of_element_located((By.XPATH, "//*[text() = 'Scan QR code to join group']")))
    assert context.browser.find_element_by_xpath("//*[text() = 'Scan QR code to join group']")

    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located(
        (By.XPATH, '//*[@id="qrCodeSubscribeGroupModal___BV_modal_body_"]/div[2]/div/canvas')))
    assert context.browser.find_element_by_xpath(
        '//*[@id="qrCodeSubscribeGroupModal___BV_modal_body_"]/div[2]/div/canvas')

    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
'//*[@id="qrCodeSubscribeGroupModal___BV_modal_body_"]/div[3]/button')))
    assert context.browser.find_element_by_xpath(
        '//*[@id="qrCodeSubscribeGroupModal___BV_modal_body_"]/div[3]/button')

@when(u'user click to the By Email or Phone button')
def step_impl(context):
        WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[text() = 'By Email or Phone']")))
        submitButton = context.browser.find_element_by_xpath("//*[text() = 'By Email or Phone']")
        assert submitButton
        submitButton.click()

@when(u'user entered valid friends name on the Invite Friends menu')
def step_impl(context):

    context.browser.find_element_by_css_selector('input[name="group-name"]').clear()
    assert context.browser.find_element_by_css_selector('input[name="group-name"]')
    context.browser.find_element_by_css_selector('input[name="group-name"]').send_keys("group1 group1")

@when(u'user entered valid email on the Invite Friends menu')
def step_impl(context):
    context.browser.find_element_by_css_selector('input[name="group-email"]').clear()
    assert context.browser.find_element_by_css_selector('input[name="group-email"]')
    context.browser.find_element_by_css_selector('input[name="group-email"]').send_keys("autotests.quicklert@gmail.com")
@when(u'user click to invite button on the Invite Friends menu')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
"/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/button")))
    submitButton = context.browser.find_element_by_xpath("/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/button")
    assert submitButton
    submitButton.click()

#@then(u'Verify displayed pop up about a successful added user')
#def step_impl(context):
#    WebDriverWait(context.browser, 20).until(
#        EC.presence_of_element_located((By.XPATH, "//*[text() = 'User invited successfully']")))
#    assert context.browser.find_element_by_xpath("//*[text() = 'User invited successfully']")

@then(u'Verify displayed user on the list invited')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, '//*[@id="group-tabs"]//div[text()="group1 group1"]')))
    assert context.browser.find_element_by_xpath('//*[@id="group-tabs"]//div[text()="group1 group1"]')

@when(u'user entered valid phone on the Invite Friends menu')
def step_impl(context):
    context.browser.find_element_by_id('phone').clear()
    assert context.browser.find_element_by_id('phone')
    context.browser.find_element_by_id('phone').send_keys("0000000000")
@when(u'user click to the Send button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH, "// *[text() = 'Send']")))
    submitButton = context.browser.find_element_by_xpath("// *[text() = 'Send']")
    assert submitButton
    submitButton.click()

@when(u'user entered Subject "Test Message Subject" and message "Test Message"')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
    'input[placeholder="Type your Subject here (Required)"]')))
    context.browser.find_element_by_css_selector('input[placeholder="Type your Subject here (Required)"]').clear()
    assert context.browser.find_element_by_css_selector('input[placeholder="Type your Subject here (Required)"]')
    context.browser.find_element_by_css_selector('input[placeholder="Type your Subject here (Required)"]').send_keys(
        "Test Message Subject " + str(new_count))
   # time.sleep(1)

    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
    'textarea[placeholder="Type your message here"]')))
    context.browser.find_element_by_css_selector('textarea[placeholder="Type your message here"]').clear()
    assert context.browser.find_element_by_css_selector('textarea[placeholder="Type your message here"]')
    context.browser.find_element_by_css_selector('textarea[placeholder="Type your message here"]').send_keys(
        "Test Message " + str(new_count))
    # time.sleep(1)

@when(u'user click to the Send icon')
def step_impl(context):

    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,'div[title="Send"]')))
    submitButton = context.browser.find_element_by_css_selector('div[title="Send"]')
    assert submitButton
    submitButton.click()

@then(u'user come to feed page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.ID,'messages')))
    assert context.browser.find_element_by_id('messages')

@then(u'new message is displayed')
def step_impl(context):
    #time.sleep(3)
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,"//*[text() = 'Test Message " + str(new_count) + "']")))
    assert context.browser.find_element_by_xpath("//*[text() = 'Test Message " + str(new_count) + "']")


@when(u'user click to search icon')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="contacts"]/div/div[1]/div[2]/div/div[2]/div')))
    submitButton = context.browser.find_element_by_xpath('//*[@id="contacts"]/div/div[1]/div[2]/div/div[2]/div')
    assert submitButton
    submitButton.click()


@when(u'user enter group name to the search field')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
                                                                               'input[placeholder="Search..."]')))
    context.browser.find_element_by_css_selector('input[placeholder="Search..."]').clear()
    assert context.browser.find_element_by_css_selector('input[placeholder="Search..."]')
    context.browser.find_element_by_css_selector('input[placeholder="Search..."]').send_keys(
        "Auto test group " + str(new_count))

@then(u'verify displaying group')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
    "//*[text() = 'Auto test group " + str(new_count) + "']")))
    assert context.browser.find_elements_by_xpath("//*[text() = 'Auto test group " + str(new_count) + "']")

@when(u'user click to delete icon on the group page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'div[title = "Delete Group"]')))
    submitButton = context.browser.find_element_by_css_selector('div[title = "Delete Group"]')
    assert submitButton
    submitButton.click()

@when(u'user confirm delete group')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.ID, 'deleteModal___BV_modal_content_')))

    submitButton = context.browser.find_element_by_xpath('//*[@id="deleteModal___BV_modal_body_"]/div[2]/button[1]')
    assert submitButton
    submitButton.click()

@then(u'verify group is not displayed')
def step_impl(context):

    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.ID, 'groups-title')))

    context.browser.find_element_by_xpath('//*[@id="contacts"]/div/div[1]/div[2]/div/div[2]/div').click()
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'input[placeholder="Search..."]')))
    #time.sleep(4)
    context.browser.find_element_by_css_selector('input[placeholder="Search..."]').clear()
    assert context.browser.find_element_by_css_selector('input[placeholder="Search..."]')
    context.browser.find_element_by_css_selector('input[placeholder="Search..."]').send_keys(
        "Auto test group " + str(new_count))
    #time.sleep(4)
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CLASS_NAME, 'not-found')))
    assert context.browser.find_element_by_class_name('not-found')
###
##
###
@when(u'user click to share many on the group page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[text() = 'Share With']")))
    submitButton = context.browser.find_element_by_xpath("//*[text() = 'Share With']")
    assert submitButton
    submitButton.click()

@when(u'user click to share icon on the group page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,'div[title ="Share"]')))
    submitButton = context.browser.find_element_by_xpath('//*[@id="group-tabs"]/div[2]/div/div[1]/div')
    assert submitButton
    submitButton.click()

@when(u'user enter the user data "{user_name}"')
def step_impl(context, user_name):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.ID, "share-with-input")))
    context.browser.find_element_by_id("share-with-input").clear()
    assert context.browser.find_element_by_id("share-with-input")
    context.browser.find_element_by_id("share-with-input").send_keys(user_name)

@when(u'user click to search icon on the share menu')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'img[class="img-15"]')))
    submitButton = context.browser.find_element_by_css_selector('img[class="img-15"]')
    assert submitButton
    submitButton.click()

@when(u'user click to execute checkbox on the share menu')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH, "// *[text() = 'Execute']")))
    submitButton = context.browser.find_element_by_xpath("// *[text() = 'Execute']")
    assert submitButton
    submitButton.click()

@then(u'verify checkboxes on the share menu with execute permission')
def step_impl(context):

    assert context.browser.find_element_by_xpath(
        '/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[3]/div/div[2]/div[1]/input').is_selected() == False


    assert context.browser.find_element_by_xpath(
        '/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[3]/div/div[2]/div[2]/input').is_selected() == True

@when(u'user click to share button on the share menu')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
     '/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/button')))

    submitButton = context.browser.find_element_by_xpath(
        '/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/button')
    assert submitButton
    submitButton.click()

@then(u'verify displayed info about shared to user2 from user1 side')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
    "//*[text()='group1 group1']")))
    assert context.browser.find_element_by_xpath("//*[text()='group1 group1']")
##
#
##
@when(u'user click to feed page')
def step_impl(context):

    submitButton = context.browser.find_element_by_css_selector("i[title = 'Home']")
    assert submitButton
    submitButton.click()

@when(u'user click to message')
def step_impl(context):
#    time.sleep(1)
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH, "//*[text()[contains(.,'" + str(new_count) +"')]]")))
    submitButton = context.browser.find_element_by_xpath("//*[text()[contains(.,'" + str(new_count) +"')]]")
    assert submitButton
    submitButton.click()
    #time.sleep(4)

@when(u'user click to Accept button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
    'button[class="btn q-btn-outline-success px-1"]')))
    submitButton = context.browser.find_element_by_css_selector(
        'button[class="btn q-btn-outline-success px-1"]')
    assert submitButton
    submitButton.click()
    #time.sleep(4)
@then(u'verify displayed the Accepted pop up')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
    "//*[text() = 'Accepted']")))
    assert context.browser.find_element_by_xpath("//*[text() = 'Accepted']")

@then(u'verify displayed the Rejected pop up')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
    "/html/body/div[3]/div/div/div/div")))
    assert context.browser.find_element_by_xpath("/html/body/div[3]/div/div/div/div")

@then(u'verify displayed the Send link')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                               "//*[text() = 'Send']")))
    assert context.browser.find_element_by_xpath("//*[text() = 'Send']")

@then(u'verify displayed the Message link')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                               "//*[text() = 'Messages']")))
    context.browser.find_element_by_xpath("//*[text() = 'Messages']")



@then(u'verify displayed the Members link')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                               "//*[text() = 'Members']")))
    assert context.browser.find_element_by_xpath("//*[text() = 'Members']")

@then(u'verify displayed the Share With link')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                               "//*[text() = 'Share With']")))
    assert context.browser.find_element_by_xpath("//*[text() = 'Share With']")

@then(u'verify displayed the Dynamic Rules link')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                               "//*[text() = 'Dynamic Rules']")))
    assert context.browser.find_element_by_xpath("//*[text() = 'Dynamic Rules']")

@then(u'verify displayed the QR icon')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
                                                                               'img[src="static/img/btns/qr.svg"]')))
    assert context.browser.find_element_by_css_selector('img[src="static/img/btns/qr.svg"]')

@when(u'user click to edit checkbox on the share menu')
def step_impl(context):

    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH, "// *[text() = 'Edit']")))
    submitButton = context.browser.find_element_by_xpath("// *[text() = 'Edit']")
    assert submitButton
    submitButton.click()

@then(u'verify checkboxes on the share menu with edit permission')
def step_impl(context):
    assert context.browser.find_element_by_xpath(
        '/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[3]/div/div[2]/div[1]/input').is_selected() == True
    assert context.browser.find_element_by_xpath(
        '/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[3]/div/div[2]/div[2]/input').is_selected() == True

@then(u'verify displayed the edit group icon')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,'div[title = "Edit Group"]')))
    assert context.browser.find_element_by_css_selector('div[title = "Edit Group"]')


@then(u'verify displayed the invite friends icon')
def step_impl(context):
    #WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
    #                                                                           'div[title="Invite Friends by Email or Phone"]')))
    assert context.browser.find_element_by_css_selector('div[title="Invite Friends by Email or Phone"]')


@when(u'user click to join icon')
def step_impl(context):

    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,'div[title = "Join Group"]')))
    submitButton = context.browser.find_element_by_css_selector('div[title = "Join Group"]')
    assert submitButton
    submitButton.click()

@when(u'user enter group "{group_name}" name on the join group page')
def step_impl(context, group_name):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
     'input[placeholder="Enter Group Name"]')))

    context.browser.find_element_by_css_selector('input[placeholder="Enter Group Name"]').clear()
    assert context.browser.find_element_by_css_selector('input[placeholder="Enter Group Name"]')
    context.browser.find_element_by_css_selector('input[placeholder="Enter Group Name"]').send_keys(group_name, " " + str(new_count))
    #time.sleep(3)

@when(u'user click to search icon on the join group page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
     '/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[2]/div')))
    submitButton = context.browser.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[2]/div')
    assert submitButton
    submitButton.click()

@when(u'user click to group with auto approve permissions on the join group page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
      "//*[text()[contains(.,'"+ str(new_count) +"')]]")))
    submitButton = context.browser.find_element_by_xpath(  "//*[text()[contains(.,'"+ str(new_count) +"')]]")
    assert submitButton
    submitButton.click()
@when(u'user click to join button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                  '/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[1]/div/button')))
    submitButton = context.browser.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[2]/div/div[2]/div/div[1]/div/button')
    assert submitButton
    submitButton.click()
    #time.sleep(4)
@then(u'verify displayed pop up about success join to group')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[text() = 'The request to join has been sent']")))
    assert context.browser.find_element_by_xpath("//*[text() = 'The request to join has been sent']")

@then(u'verify displaying joined group with auto approve permissions')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[text()[contains(.,'"+ str(new_count) +"')]]")))
    assert context.browser.find_element_by_xpath("//*[text()[contains(.,'"+ str(new_count) +"')]]")

@when(u'user select group with auto approve permissions on the group menu')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[text() = 'TestGroupAutoApprove']")
    assert submitButton
    submitButton.click()

@when(u'user click to leave icon on the group page')
def step_impl(context):
    #time.sleep(4)
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'img[src="static/img/btns/leave-group.svg"]')))
    submitButton = context.browser.find_element_by_css_selector('img[src="static/img/btns/leave-group.svg"]')
    assert submitButton
    submitButton.click()

@when(u'user click to leave button on the group page')
def step_impl(context):
    submitButton = context.browser.find_element_by_xpath("//*[text() = 'Leave']")
    assert submitButton
    submitButton.click()
@then(u'verify group with auto approve permissions is not displayed')
def step_impl(context):
    context.browser.find_element_by_xpath('//*[@id="contacts"]/div/div[1]/div[2]/div/div[2]/div').click()
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'input[placeholder="Search..."]')))
    context.browser.find_element_by_css_selector('input[placeholder="Search..."]').clear()
    assert context.browser.find_element_by_css_selector('input[placeholder="Search..."]')
    context.browser.find_element_by_css_selector('input[placeholder="Search..."]').send_keys("TestGroupAutoApprove")
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CLASS_NAME, 'not-found')))
    assert context.browser.find_element_by_class_name('not-found')
@when(u'user click to close button on the pop up')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, "button[aria-label = 'Close']")))
    submitButton = context.browser.find_element_by_css_selector("button[aria-label = 'Close']")
    assert submitButton
    submitButton.click()
@when(u'user click to delete icon on the members page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'img[src="static/img/trash-icon.svg"]')))
    submitButton = context.browser.find_element_by_css_selector('img[src="static/img/trash-icon.svg"]')
    assert submitButton
    submitButton.click()

@when(u'user confirm delete member from a group')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.ID, 'deleteMembersModal___BV_modal_body_')))
    submitButton = context.browser.find_element_by_xpath("//*[text()='Ok']")
    assert submitButton
    submitButton.click()
@then(u'verify displaying empty list of members on the members page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, 'div[class="text-center text-gray-md"]')))
    assert context.browser.find_element_by_css_selector('div[class="text-center text-gray-md"]')
@then(u'verify displaying the user 2')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[text()= 'Group2 Group2']")))
    assert context.browser.find_element_by_xpath("//*[text()= 'Group2 Group2']")

@then(u'verify not displaying the user 1')
def step_impl(context):
    #time.sleep(2)
    text1 = context.browser.find_elements_by_xpath('//div[@class="text-dark text-dots"]')
    list1 = []
    for i in text1:
        text = i.text
        list1.append(text)
    my_list = ["Group2 Group2"]
    result = list(set(my_list) - set(list1))
    print(result)
    assert "Group1 Group1" not in result
    #assert "Group2 Group2" not in list1

@then(u'verify displayed the delete group icon')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'div[title = "Delete Group"]')))
    assert context.browser.find_element_by_css_selector('div[title = "Delete Group"]')

@when (u'user entered long value to the phone input')
def step_impl(context):
    context.browser.find_element_by_id('phone').clear()
    assert context.browser.find_element_by_id('phone')
    context.browser.find_element_by_id('phone').send_keys('1111111111111111111111111')

@then(u'verify value on the phone input')
def step_impl(context):

    text = context.browser.find_element_by_id('phone').get_attribute('value')
    assert_that(text, equal_to('1111111111'))



@then(u'verify if Send link is not displayed')
def step_impl(context):
    #time.sleep(2)
    text1 = context.browser.find_elements_by_xpath("//div[@class='tab']")
    text2 = context.browser.find_elements_by_xpath("//div[@class='tab active']")
    list2 = []
    for i in text1:
        text = i.text
        list2.append(text)
        # print(text)
    for i in text2:
        text = i.text
        list2.append(text)
        # print(text)
    my_list = ['All', 'My', 'Joined', 'Paging', "Send", 'Messages', 'Members', "Share With", "Dynamic Rules"]
    print(list2)
    #if "Members" in list2:
    #    print('Test passed')
    #else:
    #    print('failed')
    #print(my_list)
    result = list(set(my_list)-set(list2))
    print(result)
    assert "Members" not in list2
    assert "Send" not in list2
    assert "Share With" not in list2
    assert "Dynamic Rules" not in list2

@when(u'user click to group with allow subscription permission on the join group page')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
     "//*[text()[contains(.,'"+ str(new_count) +"')]]")))
    submitButton = context.browser.find_element_by_xpath("//*[text()[contains(.,'"+ str(new_count) +"')]]")
    assert submitButton
    submitButton.click()

@then(u'verify displayed pop up about request to join to group')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.presence_of_element_located((By.XPATH, "//*[text() = 'The request to join has been sent']")))
    assert context.browser.find_element_by_xpath("//*[text() = 'The request to join has been sent']")

@when(u'user select request to join to the group')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
     "//*[text()[contains(.,'"+ str(new_count) +"')]]")))
    submitButton = context.browser.find_element_by_xpath("//*[text()[contains(.,'Auto test group " + str(new_count) + "')]]")
    assert submitButton
    submitButton.click()
    #time.sleep(4)
@then(u'displaying the Accept button on the request message')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
     '//*[@id="scrollable-messages-list"]/div/div[2]/div/div/button[1]')))
    assert context.browser.find_element_by_xpath('//*[@id="scrollable-messages-list"]/div/div[2]/div/div/button[1]')
@then(u'displaying the Reject button on the request message')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
     '//*[@id="scrollable-messages-list"]/div/div[2]/div/div/button[2]')))
    assert context.browser.find_element_by_xpath('//*[@id="scrollable-messages-list"]/div/div[2]/div/div/button[2]')
@when(u'user click to Reject button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
    'button[class="btn q-btn-outline-danger px-1 ml-sm"]')))
    submitButton = context.browser.find_element_by_css_selector(
        'button[class="btn q-btn-outline-danger px-1 ml-sm"]')
    assert submitButton
    submitButton.click()

@then(u'verify message is not displayed')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'img[src="static/img/btns/search.svg"]')))
    context.browser.find_element_by_css_selector('img[src="static/img/btns/search.svg"]').click()

    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'input[placeholder="Search..."]')))
    context.browser.find_element_by_css_selector('input[placeholder="Search..."]').clear()
    assert context.browser.find_element_by_css_selector('input[placeholder="Search..."]')
    context.browser.find_element_by_css_selector('input[placeholder="Search..."]').send_keys("Test Message Subject " + str(new_count))

    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CLASS_NAME, 'not-found')))
    assert context.browser.find_element_by_class_name('not-found')

@when(u'user click to Edit icon')
def step_impl (context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
    'div[title = "Edit Group')))
    submitButton = context.browser.find_element_by_css_selector(
        'div[title = "Edit Group')
    assert submitButton
    submitButton.click()
    #time.sleep(5)

@when(u'user enter new group name on the edit window')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
    'input[placeholder="Enter Group Name (1-50 characters)"]')))
    context.browser.find_element_by_css_selector('input[placeholder="Enter Group Name (1-50 characters)"]').clear()
    assert context.browser.find_element_by_css_selector('input[placeholder="Enter Group Name (1-50 characters)"]')
    context.browser.find_element_by_css_selector('input[placeholder="Enter Group Name (1-50 characters)"]').send_keys(
        "New Auto test group name " + str(new_count))
    #time.sleep(4)
@when(u'user enter new group description on the edit window')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
    'input[placeholder="Enter Group Description (1-50 characters)"]')))
    context.browser.find_element_by_css_selector('input[placeholder="Enter Group Description (1-50 characters)"]').clear()
    assert context.browser.find_element_by_css_selector('input[placeholder="Enter Group Description (1-50 characters)"]')
    context.browser.find_element_by_css_selector('input[placeholder="Enter Group Description (1-50 characters)"]').send_keys(
        "New Auto test group description " + str(new_count))

    #time.sleep(4)
@when(u'user upload picture on the edit window')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
     '//*[@id="pick-picture"]')))
    context.browser.find_element_by_css_selector('input[class="avatar-cropper-img-input"]').send_keys("D:\\Auto_test\\_1Test_img.jpg")

@when(u'user save image for avatar')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
     'div[class="avatar-cropper-container"]')))
    submitButton = context.browser.find_element_by_xpath('//*[text()="Ok"]')
    assert submitButton
    submitButton.click()
    time.sleep(3)
    #WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located(By.CSS_SELECTOR, 'img[class="rounded-circle"]'))

@when(u'user save edited  group')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
     'button[type="submit"]')))
    submitButton = context.browser.find_element_by_css_selector('button[type="submit"]')
    assert submitButton
    submitButton.click()
    #WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CLASS_NAME, 'rounded-circle')))

@then(u'verify displaying new group name')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(
        EC.visibility_of_element_located((By.XPATH, "//*[text() = 'New Auto test group name " + str(new_count) + "'][1]")))
    assert context.browser.find_element_by_xpath( "//*[text() = 'New Auto test group name " + str(new_count) + "'][1]")



@then(u'verify displaying new Group description')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,"//*[text() = 'New Auto test group description " + str(new_count) + "']")))
    assert context.browser.find_element_by_xpath("//*[text() = 'New Auto test group description " + str(new_count) + "']")

@then(u'verify displaying new avatar')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
     "div[class = 'avatar mr-sm']")))
    assert context.browser.find_element_by_css_selector("div[class = 'avatar mr-sm']")

@then(u'verify displaying the save button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
     'button[class="btn q-btn-default d-flex"]')))
    assert context.browser.find_element_by_css_selector('button[class="btn q-btn-default d-flex"]')

@then(u'verify displaying the group name input')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.ID,
                                                                               'group-name')))
    assert context.browser.find_element_by_id('group-name')

@then(u'verify displaying the group description input')
def step_impl(context):
        WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.ID,
                                                                                   'group-desc')))
        assert context.browser.find_element_by_id('group-desc')

@then(u'verify displaying the change picture permission')
def step_impl(context):

        WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.ID,
                                                                                   'pick-picture')))
        assert context.browser.find_element_by_id('pick-picture')

        #WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
        #                                                                           'input[class="avatar-cropper-img-input"]')))
        assert context.browser.find_element_by_css_selector('input[class="avatar-cropper-img-input"]')

@then (u'verify displaying the share icon')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
                                                                              'img[src="static/img/btns/share.svg"]')))
    assert context.browser.find_element_by_css_selector('img[src="static/img/btns/share.svg"]')

@then(u'verify displaying the Allow Subscription checkbox')
def step_impl(context):
    #WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.NAME,
    #                                                                          'auto-subscribe-checkbox')))
    assert context.browser.find_element_by_id('auto-subscribe-checkbox')

@then(u'verify displaying the Auto Approve checkbox')
def step_impl(context):
    #WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.NAME,
    #                                                                           'auto-approve-checkbox')))
    assert context.browser.find_element_by_id('auto-approve-checkbox')

@when(u'user entered invalid email {email} on the Invite Friends menu (Outline)')
def step_impl(context, email):
    if email == "spaces":
        context.browser.find_element_by_css_selector('input[name="group-email"]').clear()
        assert context.browser.find_element_by_css_selector('input[name="group-email"]')
        context.browser.find_element_by_css_selector('input[name="group-email"]').send_keys('   ')
        time.sleep(3)
    else:
        context.browser.find_element_by_css_selector('input[name="group-email"]').clear()
        assert context.browser.find_element_by_css_selector('input[name="group-email"]')
        context.browser.find_element_by_css_selector('input[name="group-email"]').send_keys(email)
    #time.sleep(2)
@when(u'user entered empty email data')
def  step_impl(context):
    context.browser.find_element_by_css_selector('input[name="group-email"]').clear()
    assert context.browser.find_element_by_css_selector('input[name="group-email"]')
    #time.sleep(4)
@when(u'user entered empty phone data')
def  step_impl(context):
    context.browser.find_element_by_css_selector('input[placeholder="Enter Phone Number (Max 10 digits)"]').clear()
    assert context.browser.find_element_by_css_selector('input[placeholder="Enter Phone Number (Max 10 digits)"]')
    #time.sleep(4)

@when(u'user entered valid phone {phone} on the Invite Friends menu (Outline)')
def step_impl(context, phone):

    if phone == "spaces":
        context.browser.find_element_by_css_selector('input[placeholder="Enter Phone Number (Max 10 digits)"]').clear()
        assert context.browser.find_element_by_css_selector('input[placeholder="Enter Phone Number (Max 10 digits)"]')
        context.browser.find_element_by_css_selector(
            'input[placeholder="Enter Phone Number (Max 10 digits)"]').send_keys("      ")
        #time.sleep(10)
    else:
        context.browser.find_element_by_css_selector('input[placeholder="Enter Phone Number (Max 10 digits)"]').clear()
        assert context.browser.find_element_by_css_selector('input[placeholder="Enter Phone Number (Max 10 digits)"]')
        context.browser.find_element_by_css_selector('input[placeholder="Enter Phone Number (Max 10 digits)"]').send_keys(phone)
    #time.sleep(2)

@then(u'Verify displayed error text for email input')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                               "//*[text()='* Incorrect format']")))
    assert context.browser.find_element_by_xpath("//*[text()='* Incorrect format']")

@then(u'Verify displayed error text for phone input')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                               '//*[text()="* Min length: 9 digits, Max length: 10 digits"]')))
    assert context.browser.find_element_by_xpath('//*[text()="* Min length: 9 digits, Max length: 10 digits"]')

@then(u'Verify disabled the Invite button')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
                                                                               'button[disabled="disabled"]')))
    assert context.browser.find_element_by_css_selector('button[disabled="disabled"]')



@then(u'Verify displaying max length')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                               "//*[text()='* Max 50 characters']")))
    assert context.browser.find_element_by_xpath("//*[text()='* Max 50 characters']")

@when(u'user click to the all tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                               "//*[text() = 'All']"))).click()
    assert context.browser.find_element_by_xpath("//*[text() = 'All']")


@when(u'user click to the my tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                               "//*[text() = 'My']"))).click()
    assert context.browser.find_element_by_xpath("//*[text() = 'My']")
    #time.sleep(4)

@when(u'user click to the Members tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                               "//*[text() = 'Members']"))).click()
    assert context.browser.find_element_by_xpath("//*[text() = 'Members']")
@when(u'user click to the Message tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                               "//*[text() = 'Messages']")))
    submitButton = context.browser.find_element_by_xpath("//*[text() = 'Messages']")
    assert submitButton
    submitButton.click()
    #time.sleep(5)
@then(u'new message is displayed for the Message tab')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                                "//*[text()[contains(., 'Test Message Subject "+ str(new_count) +"')]]")))

    assert context.browser.find_element_by_xpath("//*[text()[contains(., 'Test Message Subject "+ str(new_count) +"')]]")
    #time.sleep(4)

@then(u'new group should be displayed on the group list')
def disp_group(context):
    WebDriverWait(context.browser, 20).until(
        EC.visibility_of_element_located((By.XPATH, "//*[text() = 'Auto test group " + str(new_count) + "']")))
    assert context.browser.find_elements_by_xpath("//*[text() = 'Auto test group " + str(new_count) + "']")
    #group_id = context.browser.find_elements_by_xpath("/html/body/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/div[1]/div/div[1]/div[2]/span[2]")
   # open_subsc_page(group_id)

#@given(u'user open the group subscribe page')
#def open_subsc_page(context):
#    context.browser.get(
#        "https://noctdips03.quicklert.com/connect/#/group-subscribe?udata=autotests.quicklert%40gmail.com&gc=")
#    WebDriverWait(context.browser, 20).until(EC.presence_of_element_located((By.ID, 'username')))
#
#    time.sleep(5)

@then(u'verify displayed the pending text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                               "//*[text()='Pending']")))

    assert context.browser.find_element_by_xpath("//*[text()='Pending']")
@then(u'verify displaying the group subscribe page')
def step_impl(context):
    #time.sleep(5)
    context.browser.switch_to_window(context.browser.window_handles[1])
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                               "//*[text() = 'Group Subscribe']")))
    assert context.browser.find_element_by_xpath("//*[text() = 'Group Subscribe']")
    #assert context.browser.find_element_by_id("inputFirstName")
    #time.sleep(5)

@when(u'user enter group subscription data')
def step_impl(context):
  #  time.sleep(5)
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.ID,
                                                                             "inputFirstName")))
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.ID,
                                                                             "inputLastName")))
    context.browser.find_element_by_id("inputFirstName").clear()
    assert context.browser.find_element_by_id("inputFirstName")
    context.browser.find_element_by_id("inputFirstName").send_keys("group1")

    context.browser.find_element_by_id("inputLastName").clear()
    assert context.browser.find_element_by_id("inputLastName")
    context.browser.find_element_by_id("inputLastName").send_keys("group1")
    #time.sleep(2)

@then(u'verify displaying the data on the email input')
def step_impl(context):
    text = context.browser.find_element_by_id('inputEmail').get_attribute('value')
    assert_that(text, equal_to('autotests.quicklert@gmail.com'))

@when(u'user click to Subscribe button')
def step_impl(context):
    submitButton = context.browser.find_element_by_css_selector('button[class = "btn q-btn-default"]')
    assert submitButton
    submitButton.click()
@then(u'verify displaying successful text')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.XPATH,
                                                                               "//*[text()='You joined to the group successfully']")))

    assert context.browser.find_element_by_xpath("//*[text()='You joined to the group successfully']")

@then(u'verify displaying the successful pop up')
def step_impl(context):
    WebDriverWait(context.browser, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR,
                                                                               'div[class="b-toast b-toast-append b-toast-success"]')))

    assert context.browser.find_element_by_css_selector('div[class="b-toast b-toast-append b-toast-success"]')

@then(u'verify not displayed the pending test')
def step_impl(context):
    text1 = context.browser.find_elements_by_xpath('//a[@class="d-flex email"]')
    list1 = []
    for i in text1:
        text = i.text
        list1.append(text)
    my_list = ["Empty"]
    result = list(set(my_list) - set(list1))
    print(result)
    assert "Pending" not in result